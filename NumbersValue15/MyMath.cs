﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NumbersValue15
{
    class MyMath
    {
        public byte num = 50;
        private double initial;
        private string name;
        public double Num
        {
            get
            {
                return this.num;
            }
        }
        public double Initial
        {
            get
            {
                return this.initial;
            }
            set
            {
                this.initial = value;
            }
        }
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        public MyMath(double value, string name)
        {
            this.Initial = value;
            this.Name = name;
        }
        virtual public StringBuilder asLimit()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(String.Format("Calculating {0} as Limit:", this.Name));
            s.AppendLine(String.Format("Value of {0} in C#: {1:F15}", this.Name, this.Initial));

            return s;
        }
        virtual public StringBuilder asSeriesSum()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(String.Format("Calculating {0} as Sum of Series:", this.Name));
            s.AppendLine(String.Format("Value of {0} in C#: {1:F15}", this.Name, this.Initial));

            return s;
        }
        virtual public StringBuilder asIntegral()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(String.Format("Calculating {0} as Integral:", this.Name));
            s.AppendLine(String.Format("Value of {0} in C#: {1:F15}", this.Name, this.Initial));

            return s;
        }
        virtual public StringBuilder asSequence()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(String.Format("Calculating {0} as Sequence:", this.Name));
            s.AppendLine(String.Format("Value of {0} in C#: {1:F15}", this.Name, this.Initial));

            return s;
        }
        virtual public StringBuilder asConst()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(String.Format("Calculating {0} as Const:", this.Name));
            s.AppendLine(String.Format("Value of {0} in C#: {1:F15}", this.Name, this.Initial));

            return s;
        }
        public double Factorial(double i)
        {
            if (i <= 1)
                return 1;
            return i * Factorial(i - 1);
        }
        public double Acoth(double x)
        {
            return (Math.Log((x + 1) / (x - 1)) / 2);
        }
    }
    class Exp : MyMath
    {
        // http://www.wolframalpha.com/input/?i=e&rawformassumption=%7B%22C%22,+%22e%22%7D+-%3E+%7B%22NamedConstant%22%7D
        // https://ru.wikipedia.org/wiki/E_(%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
        public Exp(double value = Math.E, string name = "E") : base(value, name) { }
        public override StringBuilder asSeriesSum()
        {
            StringBuilder s = base.asSeriesSum();
            double e = 0;

            for (int i = 0; i < base.Num; i++)
            {
                e += 1 / (base.Factorial(i));
            }
            s.AppendLine(String.Format("Result as SeriesSum: {0:F15}", e));

            return s;
        }
        public override StringBuilder asLimit()
        {
            StringBuilder s = base.asLimit();
            double e = 0;

            e = Math.Pow(1 + (1 / base.Num), base.Num);
            s.AppendLine(String.Format("Result as Limit: {0:F15}", e));

            return s;
        }
    }
    class Gamma : MyMath
    {
        // http://www.wolframalpha.com/input/?i=gamma&rawformassumption=%7B%22C%22,+%22gamma%22%7D+-%3E+%7B%22NamedConstant%22%7D&rawformassumption=%7B%22DPClash%22,+%22CharacterE%22,+%22gamma%22%7D+-%3E+%7B611%7D
        // https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%BD%D0%B0%D1%8F_%D0%AD%D0%B9%D0%BB%D0%B5%D1%80%D0%B0_%E2%80%94_%D0%9C%D0%B0%D1%81%D0%BA%D0%B5%D1%80%D0%BE%D0%BD%D0%B8
        public Gamma(double value = Double.NaN, string name = "Gamma") : base(value, name) { }
        public override StringBuilder asSeriesSum()
        {
            StringBuilder s = base.asSeriesSum();
            double gamma = 0;

            for (int i = 1; i < base.Num; i++)
                gamma += (1 / i) - Math.Log((i + 1) / i);

            s.AppendLine(String.Format("Result as SeriesSum: {0:F15}", gamma));

            return s;
        }
    }
    class RootOf2 : MyMath
    {
        // http://www.wolframalpha.com/input/?i=sqrt%5B2%5D
        // https://ru.wikipedia.org/wiki/%D0%9A%D0%B2%D0%B0%D0%B4%D1%80%D0%B0%D1%82%D0%BD%D1%8B%D0%B9_%D0%BA%D0%BE%D1%80%D0%B5%D0%BD%D1%8C_%D0%B8%D0%B7_2
        public RootOf2(double value = 0, string name = "RootOf2") : base(value, name) { this.Initial = Math.Sqrt(2); }
        public override StringBuilder asSequence()
        {
            StringBuilder s = base.asSequence();
            double root = 3.0 / 2;

            for (int i = 2; i < base.Num; i++)
                root = root / 2 + 1 / root;

            s.AppendLine(String.Format("Result as Sequence: {0:F15}", root));

            return s;
        }
        public override StringBuilder asConst()
        {
            StringBuilder s = base.asConst();
            double root = 2 * Math.Cos(45 * Math.PI / 180);

            s.AppendLine(String.Format("Result as Const: {0:F15}", root));

            return s;
        }
    }
    class Pi : MyMath
    {
        // http://www.wolframalpha.com/input/?i=pi&rawformassumption=%7B%22C%22,+%22pi%22%7D+-%3E+%7B%22NamedConstant%22%7D
        // https://ru.wikipedia.org/wiki/%D0%9F%D0%B8_(%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
        public Pi(double value = Math.PI, string name = "Pi") : base(value, name) { }
        public override StringBuilder asSeriesSum()
        {
            StringBuilder s = base.asSeriesSum();
            double pi = 0;

            for (int i = 0; i < base.Num; i++)
                pi += Math.Pow(-1, i) / (2 * i + 1);
            pi *= 4;

            s.AppendLine(String.Format("Result as SeriesSum: {0:F15}", pi));

            return s;
        }
        public override StringBuilder asConst()
        {
            StringBuilder s = base.asConst();
            double pi;

            pi = Math.Acos(-1);
            s.AppendLine(String.Format("Result as Const: {0:F15}", pi));

            return s;
        }
    }
    class Ln2 : MyMath
    {
        // http://www.wolframalpha.com/input/?i=ln
        // https://ru.wikipedia.org/wiki/%D0%9D%D0%B0%D1%82%D1%83%D1%80%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9_%D0%BB%D0%BE%D0%B3%D0%B0%D1%80%D0%B8%D1%84%D0%BC
        public Ln2(double value = 0, string name = "Ln2") : base(value, name) { this.Initial = Math.Log(2); }
        public override StringBuilder asSeriesSum()
        {
            StringBuilder s = base.asSeriesSum();
            double ln2 = 0;

            for (int i = 1; i < base.Num; i++)
                ln2 += Math.Pow(-1, (i + 1))/i;

            s.AppendLine(String.Format("Result as SeriesSum: {0:F15}", ln2));

            return s;
        }
        public override StringBuilder asConst()
        {
            StringBuilder s = base.asConst();
            double ln2;

            ln2 = 2 * base.Acoth(3);
            s.AppendLine(String.Format("Result as Const: {0:F15}", ln2));

            return s;
        }
    }
}
