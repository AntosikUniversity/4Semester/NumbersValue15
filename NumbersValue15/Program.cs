﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumbersValue15
{
    class Program
    {
        static void Main(string[] args)
        {
            var e = new Exp();
            Console.WriteLine(e.asSeriesSum());
            Console.WriteLine(e.asLimit());
            Console.WriteLine("----------------");
            var gamma = new Gamma();
            Console.WriteLine(gamma.asSeriesSum());
            Console.WriteLine("----------------");
            var root2 = new RootOf2();
            Console.WriteLine(root2.asSequence());
            Console.WriteLine(root2.asConst());
            Console.WriteLine("----------------");
            var pi = new Pi();
            Console.WriteLine(pi.asSeriesSum());
            Console.WriteLine(pi.asConst());
            Console.WriteLine("----------------");
            var ln2 = new Ln2();
            Console.WriteLine(ln2.asSeriesSum());
            Console.WriteLine(ln2.asConst());


            Console.ReadKey();
        }
    }
}
